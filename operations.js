const fs = require('fs');

// Créer un fichier nommé "welcome.txt" et écrire "Hello Node" à l'intérieur
fs.writeFile('welcome.txt', 'Hello Node', (err) => {
  if (err) throw err;
  console.log('Le fichier welcome.txt a été créé avec succès.');
});

// Lire les données du fichier "welcome.txt" et afficher le contenu
fs.readFile('welcome.txt', 'utf8', (err, data) => {
  if (err) throw err;
  console.log('Contenu du fichier welcome.txt :');
  console.log(data);
});
